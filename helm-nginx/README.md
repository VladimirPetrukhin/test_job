# helm-nginx
Запуск локального сервера Nginx с помощью Helm и Minicube на порту 32080

# Предварительные требования

* Установить [Minikube](https://kubernetes.io/ru/docs/tasks/tools/install-minikube/)
* Установить [Helm](https://helm.sh/ru/docs/intro/install/)
* Запустить minikube

# Инструкции по установке
* Клонировать репозиторий
* Произвести установку с помощью команды 
```
helm install nginx helm-nginx/ --values helm-nginx/values.yaml
```

Инструкция по получению адреса доступа к Nginx: 
```
export POD_NAME=$(kubectl get pods -l "app.kubernetes.io/name=buildachart,app.kubernetes.io/instance=my-cherry-chart" -o jsonpath="{.items[0].metadata.name}")
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services cherry-chart)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
echo http://$NODE_IP:$NODE_PORT
```
Информация об авторе
------------------

- Электронная почта: <petrukhin.ru@yandex.ru>

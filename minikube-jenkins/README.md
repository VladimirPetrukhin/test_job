# Введение
Основная идея заключается в использовании Minikube для настройки локального кластера Kubernetes, и развернуть экземпляр Jenkins.

# Предварительные требования
Обязательными условиями для этого проекта являются наличие [Minikube](https://kubernetes.io/ru/docs/tasks/tools/install-minikube/), [kubectl](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/), [Docker](https://docs.docker.com/engine/install/#licensing).

## Развертывание Kubernetes Jenkins
**Шаг 1.** Запустите Minikube.

```shell
minikube start
```

**Шаг 2.** Перейдите в скачанный репозиторий и создайте учетную запись службы с помощью kubectl.

```shell
kubectl apply -f serviceAccount.yaml
```
**Шаг 3.** Создадайте том с помощью kubectl.

```shell
kubectl create -f volume.yaml
```

**Шаг 4.** Создайте развертывание с помощью kubectl.

```shell
kubectl apply -f deployment.yaml
```

**Шаг 5.** Создайте службу Jenkins с помощью kubectl.

```shell
kubectl apply -f service.yaml
```

**Можно произвести установку с помощью скрипта `jenkins_install.sh`**


## Доступ к Jenkins

**Узнать адрес для подлючения к Jenkins.**

**Шаг** Запустите команду вывода адреса по которому доступен Jenkins.

```shell
jsonpath="{.spec.ports[0].nodePort}"
NODE_PORT=$(kubectl get -o jsonpath=$jsonpath services jenkins-service)
jsonpath="{.items[0].status.addresses[0].address}"
NODE_IP=$(kubectl get nodes -o jsonpath=$jsonpath)
echo http://$NODE_IP:$NODE_PORT
```

**Jenkins запросит первоначальный пароль администратора при первом доступе к панели мониторинга.**   

**Шаг 1.** Используйте команду чтобы получить имя модуля.

```shell
kubectl get pods
```

**Шаг 2.** Запустите команду exec, чтобы получить пароль непосредственно из папки, указав имя модуля из предыдущего шага.

```shell
kubectl exec -it <имя модуля> cat /var/jenkins_home/secrets/initialAdminPassword
```

**Альтернативный вариант** Используя имя модуля, вы можете получать журналы, как показано ниже. Замените имя модуля на свое имя модуля.

```shell
kubectl logs <имя модуля>
```

Информация об авторе
------------------

- Электронная почта: <petrukhin.ru@yandex.ru>
